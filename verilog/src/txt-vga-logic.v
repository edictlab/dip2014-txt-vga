
module txt_vga_logic(
  input vgaclk,
  input vsync,
  input [11:0] x, 
  input on_screen,
  input [31:0] ram_data, 
  input [7:0] rom_data,
  output [ADDR_WIDTH-1:0] ram_addr,
  output [ADDR_WIDTH-1:0] rom_addr,
  output wire pixel_out
  );

  parameter BUFFER_WIDTH  = 80;
  parameter BUFFER_HEIGHT = 40;
  parameter FONT_WIDTH  = 8;  // in pixels
  parameter FONT_HEIGHT = 12; // in pixels
  parameter ADDR_WIDTH = 18;

  reg [6:0] col=0, row=0; // character on screen
  reg [5:0] dx=FONT_WIDTH-1, dy=0; // character pixel definition column and row
  reg [7:0] font_pixels = 0;;
  wire [ADDR_WIDTH:0] char_addr;
  wire [7:0] ram_ascii ;

  // RAM/ROM addresses and data in/out
  assign char_addr = row * BUFFER_WIDTH + col;
  assign ram_addr = {2'b00, char_addr[ADDR_WIDTH-1:2]};
  assign rom_addr = ram_ascii * FONT_HEIGHT + dy;

  // pixel from rom character definition
  assign pixel_out = on_screen & rom_data[dx];

  assign ram_ascii = 
        (i == 2'b11)? ram_data[7:0]   :
        (i == 2'b10)? ram_data[15:8]  :
        (i == 2'b01)? ram_data[23:16] :
        ram_data[31:24] ;

  reg [1:0] i;
  always@(posedge vgaclk)
    i <= char_addr[1:0];
 
  // dx (x pixel) and col (text buffer column)
  always @(posedge vgaclk)
  begin
    if(on_screen)  begin
      if(dx > 0) begin 
        dx <= dx - 1;
        // Two clocks delay from here to font row definition:
        // 1 clk to screen buffer and 1 clk from scr buff to font def.
        if(dx == 2)
          col <= col + 1;
      end
      else begin
        dx <= FONT_WIDTH-1;
      end
    end
    else begin
      col <= 0;
      dx <= FONT_WIDTH-1;
    end
  end

  // dy (y pixel) and row (text buffer row)
  always @(posedge vgaclk)
  begin
    if(on_screen)  begin
      if(x == BUFFER_WIDTH*FONT_WIDTH-2) begin
        if(dy < FONT_HEIGHT-1) 
        begin
          dy <= dy + 1;
        end
        else begin 
          dy <= 0;
          row <= row + 1;
        end
      end
    end
    else begin // prepare for the first row
      if(~vsync) begin
        row <= 0;
        dy <=0;
      end
    end

  end

endmodule
