// Font ROM
//

module font_rom(
  input clk,
  input re,
  input [ADDR_WIDTH-1:0] addr,
  output reg [7:0] dout
);

  parameter ADDR_WIDTH = 18;

  reg [7:0] rom [0:3071];

  initial begin
    $readmemh("font-def.data", rom);
  end

  always @(posedge clk) begin
    if (re)
      dout <= rom[addr];
  end

endmodule
